import unittest

import menu
import fileparser
from balancer import Balancer
from user import User


class TestBalancer(unittest.TestCase):

    def test_init(self):
        balancer = Balancer()
        # check the default
        self.assertEqual(balancer.users, [])
        self.assertEqual(balancer.tasks, [])
        self.assertEqual(balancer.not_assigned_tasks, [])
        self.assertFalse(balancer.balanced)

    def test_task_add(self):
        balancer = Balancer()
        balancer.task_add('TaskName', 20)
        self.assertIn({'name': 'TaskName', 'points': 20}, balancer.tasks)

        balancer.task_add('TaskName', 20)
        balancer.task_add('TaskName', 20)

        self.assertEqual(3, len(balancer.tasks))

    def test_task_add_raises(self):
        balancer = Balancer()
        self.assertRaisesRegex(AssertionError, 'Name must to be string', balancer.task_add, name=[1, 2, 3], points=1)
        self.assertRaisesRegex(AssertionError, 'Points must to be integer', balancer.task_add, name='a', points='b')

    def test_user_add(self):
        balancer = Balancer()
        balancer.user_add('UserName', 20)

        self.assertEqual('UserName', balancer.users[0].name)
        self.assertEqual(20, balancer.users[0].capacity)

        balancer.user_add('UserName', 20)
        balancer.user_add('UserName', 20)

        self.assertEqual(3, len(balancer.users))

    def test_user_add_raises(self):
        balancer = Balancer()
        self.assertRaisesRegex(AssertionError, 'Name must to be string', balancer.user_add, name=[1, 2, 3], capacity=1)
        self.assertRaisesRegex(AssertionError, 'Capacity must to be integer', balancer.user_add, name='a', capacity='b')

    def test_balance_simple(self):
        balancer = Balancer()

        balancer.user_add('John', 20)
        balancer.user_add('Constantine', 20)
        balancer.user_add('Dou', 20)

        balancer.task_add('SomeTask', 20)
        balancer.task_add('SomeOtherTask', 20)
        balancer.task_add('SomeOtherOtherTask', 20)

        balancer.balance()
        self.assertTrue(balancer.balanced)
        self.assertListEqual([20, 20, 20], [user.points for user in balancer.users])
        # Check that second balance call raises AssertionError
        self.assertRaises(AssertionError, balancer.balance)

    def test_balance_not_assigned_tasks(self):
        balancer = Balancer()

        balancer.task_add('SomeTask', 20)
        balancer.task_add('SomeOtherTask', 20)
        balancer.task_add('SomeOtherOtherTask', 20)
        balancer.balance()

        self.assertEqual(3, len(balancer.not_assigned_tasks))

    def test_balance(self):
        balancer = Balancer()

        balancer.user_add('John', 100)
        balancer.user_add('Constantine', 30)
        balancer.user_add('Dou', 50)
        balancer.user_add('Wayne', 63)
        balancer.user_add('Wick', 72)
        balancer.user_add('Nothing', 1)

        balancer.task_add('SomeTask', 13)
        balancer.task_add('SomeOtherTask', 14)
        balancer.task_add('SomeOtherOtherTask', 15)
        balancer.task_add('SomeTask', 13)
        balancer.task_add('SomeOtherTask', 14)
        balancer.task_add('SomeOtherOtherTask', 15)
        balancer.task_add('SomeOtherOtherTask', 35)
        balancer.task_add('SomeOtherOtherTask', 16)
        balancer.task_add('SomeOtherOtherTask', 16)
        balancer.task_add('SomeOtherOtherTask', 21)
        balancer.task_add('SomeOtherOtherTask', 21)
        balancer.task_add('SomeOtherOtherTask', 16)
        balancer.balance()

        self.assertEqual(48, balancer.users[0].points)
        self.assertEqual(52, balancer.users[0].capacity)

        self.assertEqual(21, balancer.users[1].points)
        self.assertEqual(9, balancer.users[1].capacity)

        self.assertEqual(49, balancer.users[2].points)
        self.assertEqual(1, balancer.users[2].capacity)

        self.assertEqual(46, balancer.users[3].points)
        self.assertEqual(17, balancer.users[3].capacity)

        self.assertEqual(45, balancer.users[4].points)
        self.assertEqual(27, balancer.users[4].capacity)

        self.assertEqual(sum([task['points'] for task in balancer.tasks]),
                         sum([user.points for user in balancer.users]))

    def test_report(self):
        balancer = Balancer()

        balancer.user_add('John', 100)
        balancer.user_add('Constantine', 30)
        balancer.user_add('Nothing', 1)
        balancer.user_add('Zero', 0)

        balancer.task_add('SomeTask', 13)
        balancer.task_add('SomeOtherTask', 14)
        balancer.task_add('SomeOtherOtherTask', 15)
        balancer.task_add('SomeOtherOtherTask', 200)

        # check it's don't report untill balance
        self.assertRaises(AssertionError, balancer.report)

        balancer.balance()
        result = balancer.report()

        self.assertIn('Not assigned tasks', result)

        for user in balancer.users:
            self.assertIn(user.name, result)
            self.assertIn(str(user.points), result)

        for task in balancer.tasks:
            self.assertIn(task['name'], result)
            self.assertIn(str(task['points']), result)


class TestFileParser(unittest.TestCase):
    users_file = 'examples/users.csv'
    tasks_file = 'examples/tasks.csv'

    def test_csv_parse_file_not_exists(self):
        with self.assertRaises(SystemExit):
            [x for x in fileparser.csv_parse('/bad/route')]

    def test_csv_parse(self):
        self.assertEqual(7, len([user for user in fileparser.csv_parse(self.users_file)]))

    def test_parse_users(self):
        balancer = Balancer()
        fileparser.users_parse(self.users_file, balancer)
        self.assertEqual(7, len(balancer.users))

    def test_parse_tasks(self):
        balancer = Balancer()
        fileparser.tasks_parse(self.tasks_file, balancer)
        self.assertEqual(19, len(balancer.tasks))


class TestUser(unittest.TestCase):

    def test_init(self):
        user = User('John', 20)
        self.assertEqual(user.name, 'John')
        self.assertEqual(user.capacity, 20)
        self.assertEqual(user.points, 0)
        self.assertEqual(user.tasks, [])

    def test_init_raises(self):
        self.assertRaises(AssertionError, User, 'John', '20')

    def test_repr(self):
        # for 100% coverage :)
        user = User('John', 20)
        self.assertEqual('{}'.format(user), 'User (name=John, capacity=20, points=0, tasks=[])')

    def test_load_percentage(self):
        user = User('John', 20)
        self.assertEqual(user.load_percentage, 0)

        user.capacity -= 10
        user.points += 10
        self.assertEqual(user.load_percentage, 0.5)

        user.capacity -= 10
        user.points += 10
        self.assertEqual(user.load_percentage, 1)

    def test_initial_capacity(self):
        user = User('John', 20)
        self.assertEqual(user.initial_capacity, 20)

        user.capacity -= 10
        user.points += 10
        self.assertEqual(user.initial_capacity, 20)

        user.capacity -= 10
        user.points += 10
        self.assertEqual(user.initial_capacity, 20)

    def test_task_assign(self):
        user = User('John', 20)
        self.assertEqual(0, len(user.tasks))

        self.assertTrue(user.task_assign({'name': 'action!', 'points': 10}))
        self.assertEqual(1, len(user.tasks))
        self.assertEqual(10, user.points)
        self.assertEqual(10, user.capacity)

        self.assertTrue(user.task_assign({'name': 'action 2!', 'points': 10}))
        self.assertEqual(2, len(user.tasks))
        self.assertEqual(20, user.points)
        self.assertEqual(0, user.capacity)

        self.assertFalse(user.task_assign({'name': 'action! 3', 'points': 10}))


if __name__ == '__main__':
    unittest.main()
