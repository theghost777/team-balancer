"""User module, containing user class."""


class User:
    """User class, handle all operations with user."""

    def __init__(self, name, capacity):
        """Initiate user object, and set defaults, assert name is str, capacity is int."""
        assert isinstance(name, str), 'Name must to be string'
        assert isinstance(capacity, int), 'Capacity must to be integer'

        self.name, self.capacity = name, capacity
        # defaults
        self.points, self.tasks = 0, []

    def __repr__(self):
        """Repr for user."""
        return '{} (name={}, capacity={}, points={}, tasks={})'.format(self.__class__.__name__,
                                                                       self.name,
                                                                       self.capacity,
                                                                       self.points,
                                                                       self.tasks)

    @property
    def load_percentage(self):
        """Return load percentage, in float."""
        # check initial_capacity to prevent division by zero
        return self.points / self.initial_capacity if self.initial_capacity else 0

    @property
    def initial_capacity(self):
        """Return initial capacity for user in integer."""
        return self.capacity + self.points

    def task_assign(self, task):
        """Assign task to user and return True, if user can handle it, otherwise return False.

        task(dict) - task to assign.
        return Bool.
        """
        if self.capacity >= task['points']:
            self.capacity -= task['points']
            self.points += task['points']
            # mark task as assigned to user
            self.tasks.append(task)
            return True
        return False
