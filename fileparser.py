"""Tools for parsing files with data for balancer."""
import os
import sys
import csv


def csv_parse(filepath):
    """Generator to iterate over csv file.

    Check file is exists, sniff csv dialect, open and close file. Handle empty file case.

    filepath(str) - path to csv file.
    """
    if not os.path.exists(filepath):
        sys.exit("{} file not exists".format(filepath))

    with open(filepath, newline='') as csvfile:
        try:
            dialect = csv.Sniffer().sniff(csvfile.read(1024))
        except csv.Error:
            # File is empty, it's okey
            return

        csvfile.seek(0)
        spamreader = csv.reader(csvfile, dialect)
        for row in spamreader:
            yield row


def users_parse(filepath, balancer):
    """Parse csv file with users.

    filepath(str) - path to csv file with users.
    balancer - Balancer instance, to add users to it.
    """
    for line_num, row in enumerate(csv_parse(filepath), 1):
        name, capacity = row

        try:
            capacity = int(capacity)
        except ValueError:
            sys.exit('file {}, line {}: Capacity must to be integer'.format(filepath, line_num))

        balancer.user_add(name, capacity)


def tasks_parse(filepath, balancer):
    """Parse csv file with tasks.

    filepath(str) - path to csv file with tasks.
    balancer - Balancer instance, to add tasks to it.
    """
    for line_num, row in enumerate(csv_parse(filepath), 1):
        name, points = row

        try:
            points = int(points)
        except ValueError:
            sys.exit('file {}, line {}: Points must to be integer'.format(filepath, line_num))

        balancer.task_add(name, points)
