# Team Balancer #

## How to Run ##
2 ways for running:
### 1. Load users and tasks from file ###
To run demo:

```
#!bash

python3 main.py -u examples/users.csv -t examples/tasks.csv
```
To get help on arguments:

```
#!shell

python3 main.py -h
```
---------------------

CSV files without header, format:

UserName,Capacity

TaskName,Points


### 2. Interactive mode ###

```
#!shell

python3 main.py
```


and follow on screen instructions
## Task ##
We have a group of users - the team whose work capacity depends on his/her capability.  We represent this amount of work as points (_an integer_).

This team needs to complete a list of tasks. The task complexity is also represented by points. The harder the task, the higher the point.

The goal of this exercise is to find the best possible way to distribute tasks among the team so that the users’ workload is as equal as possible (to avoid the case when one employee is overworked, while another has almost nothing to do).

Every task has a name, and each user has a name.

For each user, the result should display the following as a minimum:

* User's nickname
* User's personal points
* The list of all tasks assigned to the user (name and corresponding points)
* the total amount of points assigned to the user


You can structure the data the way you see fit.

Your solution should be represented as **console application** where it is possible to create users and tasks. As result the tasks should be distributed among the users and the results should be displayed.

We will be looking at code quality, methodology, approach of the problem and best practice.