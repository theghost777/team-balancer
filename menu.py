"""Console application menu and tools for interaction with user."""


def print_error(msg):
    """Print msg in red."""
    print("\033[91m{}\033[00m".format(msg))


def print_success(msg):
    """Print msg in green."""
    print("\033[92m{}\033[00m".format(msg))


def input_int(msg):
    """Ask user to input number.

    If input isn't number then asks again

    msg(string) - prompt string.
    return: int
    """
    while True:
        number = input(msg)
        try:
            number = int(number)
        except ValueError:
            print_error('Please enter number, not string')
            continue

        return number


def input_not_empty(msg):
    """Ask user to input not empty string.

    If input is empty then asks again.

    msg(string) - prompt string.
    return: string
    """
    while True:
        value = input(msg)
        if not value:
            print_error("Enter something please.")
            continue
        return value


def main_menu(balancer):
    """Main menu logic. Cares user input and call balancer methods."""
    while True:
        print("""Hello stranger, what do you want?
     1: Create user.
     2: Create task.
     3: Balance and exit.
              """)
        action = input_not_empty('> ')

        if action == '1':
            name = input_not_empty("User name: ")
            capacity = input_int('User capacity: ')
            balancer.user_add(name, capacity)
            print_success('User "{}" created.'.format(name))

        elif action == '2':
            name = input_not_empty("Task name: ")
            points = input_int('Task points: ')
            balancer.task_add(name, points)
            print_success('Task "{}" created.'.format(name))

        elif action == '3':
            return
