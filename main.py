"""Application for balancing tasks among users.

You can create tasks, users.
Balance and get result.
"""
import sys
import argparse

from balancer import Balancer
import fileparser
import menu

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--users', help='CSV file with users: user name, user_capacity')
    parser.add_argument('-t', '--tasks', help='CSV file with tasks: task name, task points')
    args = parser.parse_args()

    balancer = Balancer()

    if args.tasks and args.users:
        fileparser.tasks_parse(args.tasks, balancer)
        fileparser.users_parse(args.users, balancer)
    elif args.tasks or args.users:
        sys.exit("Sorry, if you specify files with data, you need to specify both "
                 "file paths for users and for tasks.\n\n"
                 "You can also try interactive mode, just run program without args")
    else:
        menu.main_menu(balancer)

    balancer.balance()
    print(balancer.report())
