"""Load balancer."""

from user import User

# Some table constants
TASK_MASK = '{name} ({points})'
TABLE_SPACING = [25, 10, 10, 10, 25]
TABLE_MASK = '│{{:<{}}}│{{:>{}}}│{{:>{}}}│{{:>{}}}│{{:<{}}}│'.format(*TABLE_SPACING)
TABLE_HEADER = TABLE_MASK.format('Name', 'Capacity', 'Points', 'Load %', 'Tasks  (name (points))')
ROW_DELIMITER = '├' + '┼'.join(['—' * x for x in TABLE_SPACING]) + '┤'


class Balancer:
    """Cares tasks, users, and balance tasks among users."""

    def __init__(self):
        """Initialize balancer, set up defaults."""
        self.tasks = []
        self.users = []
        self.not_assigned_tasks = []
        self.balanced = False

    def task_add(self, name, points):
        """Add task to balancer.

        name(str) - name of task
        points(int) - points for this task
        """
        assert isinstance(name, str), 'Name must to be string'
        assert isinstance(points, int), 'Points must to be integer'

        self.tasks.append({
            'name': name,
            'points': points,
        })

    def user_add(self, name, capacity):
        """Add user to balancer.

        name(str) - name of user
        capacity(int) - capacity of user. How many points user can handle
        """
        self.users.append(User(name, capacity))

    def balance(self):
        """Balance tasks among users, mark this balancer as balanced."""
        # to prevent double balancing
        assert not self.balanced

        for task in sorted(self.tasks, key=lambda task: task['points'], reverse=True):
            # maybe we need to sort users by load_percentage here ?
            for user in sorted(self.users, key=lambda user: user.points):
                if user.task_assign(task):
                    break
            else:
                self.not_assigned_tasks.append(task)
        self.balanced = True

    def report(self):
        """Construct report for balanced users.

        return string with report formatted as table for console output.
        """
        # don't report before balanced
        assert self.balanced

        rows = [ROW_DELIMITER, TABLE_HEADER]

        for user in sorted(self.users, key=lambda user: user.name):
            rows.append(ROW_DELIMITER)
            # get first task to print it in one row with user info
            first_task = TASK_MASK.format_map(user.tasks[0]) if user.tasks else ''

            # format table row with user
            row = TABLE_MASK.format(user.name, user.initial_capacity, user.points,
                                    '{:.1%}'.format(user.load_percentage), first_task)
            rows.append(row)

            # ignore first task here, because it's already added to row with user info
            for task in user.tasks[1:]:
                rows.append(TABLE_MASK.format('', '', '', '', TASK_MASK.format_map(task)))
        rows.append(ROW_DELIMITER)

        if self.not_assigned_tasks:
            rows.append('\nNot assigned tasks:')
        for task in self.not_assigned_tasks:
            rows.append(TASK_MASK.format_map(task))

        return '\n'.join(rows)
